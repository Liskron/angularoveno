angular.module('NoteWrangler').config(function($routeProvider){
	$routeProvider
		.when('/',{
			templateUrl: '/templates/pages/home/index.html'
		})
		.when('/realizacja', {
			templateUrl: '/templates/pages/realizacje/index.html'
		})
		.when('/not_found', {
			templateUrl: '/templates/pages/notfound/index.html'
		})
		.when('/kontakt',{
			templateUrl: '/templates/pages/kontakt/index.html',
			controller: 'ContactController'
		})
		.otherwise({redirectTo: '/not_found'})
});