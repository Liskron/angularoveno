(function(){
	var app = angular.module("NoteWrangler",['ngRoute']);

	app.controller('RealizationController', function(){
		this.list = listRealization;
	});


	app.controller('ContactController',['$scope', function($scope){

		$scope.email = {
			usrName: 'xlm',
			usrSecName: 'hej',
			usrEmail: 'asdf@asdf.com',
			usrTel: '46549654',
			usrMessage: 'asalskdfjlaskdfjlkj'
		};

		$scope.submit = function() {
			console.log($scope.email);
		};
	}]);

	app.run(function($rootScope,$timeout){
		$rootScope.layout = {};
		$rootScope.layout.loading = false;

		$rootScope.$on('$routeChangeStart', function () {
	        //show loading gif
	        $rootScope.layout.loading = true;         
	    });

	    $rootScope.$on('$routeChangeSuccess', function () {
	        $timeout(function(){
	          $rootScope.layout.loading = false;
	        }, 500);
	    });

	    $rootScope.$on('$routeChangeError', function () {

	        //hide loading gif
	        alert('wtff');
	        $rootScope.layout.loading = false;

	    });
	});

	var listRealization = [{
		name: 'Projekt1',
		description: {
			full: "Rozbudowany opis Projekt1",
			thumb: "Krótki opis Projekt1"
		}
	},
	{
		name: 'Projekt2',
		description: {
			full: "Rozbudowany opis Projekt2",
			thumb: "Krótki opis Projekt2"
		}
	}]
})();